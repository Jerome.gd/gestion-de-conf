# Definitions

## Plateforme de service

Ensemble cohérent d'hôtes, configuration et applicatifs produisant un ou plusieurs services informatique.

Exemple : pour un site web c'est les hôtes configuré pour supporter les serveur web et de base de données necessaire au fonctionnement du site.

## Les configurations d'une plateforme de service

C'est l'ensemble des éléments et objets déployé et modifié sur le groupe d'hôtes afin de déployer la platforme de service

Exemple : configuration de vm, system déployés, parametrage systeme de stockage, comptes, groupes, package, fichier de configuration, stucture de base de donnée etc...

## Le versionning des configurations

Ensemble de moyen mis en oeuvre afin de tracer et d'identifier toutes les modifications effectuées sur les configurations.

## Le contrôle de conformité des configurations

Activité consistant a confronté les configurations réel d'une platforme de service avec la version documenté.

## La révocation de configuration

Opération qui consiste à dé-provisioner une configuration : supprimer tout les éléments de configuration déployé par cette configuration jusqu'a remettre en état l'hôte supprotant cette configuration.

## L'idempotence

c'est la faculté qu'un processus à lorsqu'il produit le même résultat peu importe le nombre de fois ou il est exécuté.

Exemple : Le tri d'une liste peu s'appliquer plusieurs fois à cette liste. le resultat du tri restera la liste des élément trié.

## La gestion des configurations

Ensemble des techniques et de moyens permettant de garantir la conformité des configurations en production avec leur version documenté.

On garanti ainsi notre capacité à redéployer complètement une platforme de service.
